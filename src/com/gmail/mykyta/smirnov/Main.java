package com.gmail.mykyta.smirnov;

import com.gmail.mykyta.smirnov.stringFunction.LengthOfTheShortestWord;
import com.gmail.mykyta.smirnov.stringFunction.NonReapeatingCharacters;
import com.gmail.mykyta.smirnov.stringFunction.ReplaceWordsWithCharacter;
import com.gmail.mykyta.smirnov.stringFunction.WordCount;
import com.gmail.mykyta.smirnov.symbolInOneLine.*;
import org.jdom.output.support.SAXOutputProcessor;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        LengthOfTheShortestWord lengthOfTheShortestWord = new LengthOfTheShortestWord();
//        String line = "The length of the shortest word in the line, with spaces and commas.";

//        System.out.println(lengthOfTheShortestWord.shortestWord(line));

//        ReplaceWordsWithCharacter replaceWordsWithCharacter = new ReplaceWordsWithCharacter();
//        String[] str = new String[]{"aasd", "asknf", "das", "as", "dasdas", "da", "sdas", "dasdas", "das", "das", "dakls", "adukr"};
//
//        ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(replaceWordsWithCharacter.replaceWord(str,5)));
//        System.out.println(arrayList);

//        NonReapeatingCharacters nonReapeatingCharacters = new NonReapeatingCharacters();
//        String str = "qqqwwweeerrrtttyyy";
//
//        System.out.println(nonReapeatingCharacters.onlyOneRepeatCharacters(str));

//        ASCIITable asciiTable = new ASCIITable();
//        System.out.println(asciiTable.allASCIITable());

//        CapsAlphabet capsAlphabet = new CapsAlphabet();
//        System.out.println(capsAlphabet.alphabet());

//        NumberFromZeroToNine numberFromZeroToNine = new NumberFromZeroToNine();
//        System.out.println(numberFromZeroToNine.numbersFromZeroToNine());

//        ReverseAlphabet reverseAlphabet = new ReverseAlphabet();
//        System.out.println(reverseAlphabet.reverseAlphabet());

        RussianAlphabet russianAlphabet = new RussianAlphabet();
        System.out.println(russianAlphabet.russianAlphabet());

    }
}
