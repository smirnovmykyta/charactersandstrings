package com.gmail.mykyta.smirnov.symbolInOneLine;

public class ReverseAlphabet {
    public String reverseAlphabet() {
        StringBuilder reverseAlphabet = new StringBuilder();
        for (char i = 'z'; i >= 'a'; i--) {
            reverseAlphabet.append(i + " ");
        }
        return reverseAlphabet.toString();
    }
}
