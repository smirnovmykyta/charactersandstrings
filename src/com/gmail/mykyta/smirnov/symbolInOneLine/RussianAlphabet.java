package com.gmail.mykyta.smirnov.symbolInOneLine;

public class RussianAlphabet {
    public String russianAlphabet() {
        StringBuilder russianAlphabet = new StringBuilder();
        for (char i = 'а'; i <= 'я'; i++) {
            russianAlphabet.append(i + " ");
        }
        return russianAlphabet.toString();
    }
}
