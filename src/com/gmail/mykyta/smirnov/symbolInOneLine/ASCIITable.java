package com.gmail.mykyta.smirnov.symbolInOneLine;

public class ASCIITable {
    public String allASCIITable() {
        StringBuilder tableASCII = new StringBuilder();
        for (char i = ' '; i < 274; i++) {
            tableASCII.append(i + " ");
        }
        return tableASCII.toString();
    }
}
