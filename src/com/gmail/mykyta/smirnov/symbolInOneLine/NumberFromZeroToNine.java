package com.gmail.mykyta.smirnov.symbolInOneLine;

public class NumberFromZeroToNine {
    public String numbersFromZeroToNine() {
        StringBuilder numbersFromZeroToNine = new StringBuilder();
        for (char i = '0'; i <= '9'; i++) {
            numbersFromZeroToNine.append(i + " ");
        }
        return numbersFromZeroToNine.toString();
    }
}
