package com.gmail.mykyta.smirnov.symbolInOneLine;

public class CapsAlphabet {
    public String alphabet() {
        StringBuilder alphabet = new StringBuilder();
        for (char i = 'A'; i <= 'Z'; i++) {
            alphabet.append(i + " ");
        }
        return alphabet.toString();
    }
}
