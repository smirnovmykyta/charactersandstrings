package com.gmail.mykyta.smirnov.stringFunction;

import java.util.Arrays;

public class NonReapeatingCharacters {

    public StringBuilder onlyOneRepeatCharacters(String line) {
        char[]chars = line.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repeatedChar;
        for (int i = 0; i < chars.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i]== chars[j]) {
                    repeatedChar = true;
                    break;
                }
            }
            if (!repeatedChar) {
                sb.append(chars[i]);
            }
        }
        return sb;
    }
}
