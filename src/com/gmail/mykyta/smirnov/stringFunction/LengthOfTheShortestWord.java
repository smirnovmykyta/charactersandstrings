package com.gmail.mykyta.smirnov.stringFunction;

public class LengthOfTheShortestWord {

    public int shortestWord(String line) {
        String[] s = line.split("[\\p{Punct}\\p{Space}]");
        int lengthShortestWord = s[0].length();
        for (String word : s){
            if (word.length() != 0){
                if(word.length() < lengthShortestWord){
                    lengthShortestWord = word.length();
                }
            }
        }

        return lengthShortestWord;
    }
}
