package com.gmail.mykyta.smirnov.stringFunction;

public class SpaceAfterComma {

    public String space(String line){
        line = line.replace(", ", ",");
        line = line.replace(",", ", ");

        return line;
    }

}
