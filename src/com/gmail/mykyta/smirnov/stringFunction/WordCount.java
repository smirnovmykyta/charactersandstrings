package com.gmail.mykyta.smirnov.stringFunction;

public class WordCount {
    public int countingNumberWords(String line){
        int count = 0;
        String [] temp = line.split("[\\p{Space}]");
        for(String splitter : temp){
                count++;
        }

        return count;
    }
}
