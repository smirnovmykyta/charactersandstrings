package com.gmail.mykyta.smirnov.stringFunction;

public class ReverseString {
    public String reverse(String line){
        StringBuffer stringBuffer = new StringBuffer(line);
        stringBuffer.reverse();

        return stringBuffer.toString();
    }
}
