package com.gmail.mykyta.smirnov.stringFunction;

public class ReplaceWordsWithCharacter {

    public String[] replaceWord(String[] arrayWords, int wordLength){
        for (int i = 0; i < arrayWords.length; i++){
            if(arrayWords[i].length() == wordLength){
                int temp = wordLength - 3;
                arrayWords[i] = arrayWords[i].substring(0, temp) + "$";
            }
        }
        return arrayWords;
    }
}
