package com.gmail.mykyta.smirnov.stringFunction;

import java.util.Arrays;

public class DeleteLastWordInString {
    public String deleteLastWord(String line){
        String [] str = line.split("[\\p{Space}]");
        str[str.length - 1] = " ";
        line = Arrays.toString(str);
        line = line.replace(", ", " ").replace("[", "").replace("]", "").trim();

        return line;
    }
}
