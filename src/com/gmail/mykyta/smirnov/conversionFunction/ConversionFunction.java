package com.gmail.mykyta.smirnov.conversionFunction;

public class ConversionFunction {
    public String integerToString(int input){
        return String.valueOf(input);
    }

    public String doubleToString(double input){
        return String.valueOf(input);
    }

    public int stringToInteger(String input){
        return Integer.parseInt(input);
    }

    public double stringToDouble(String input){
        return Double.parseDouble(input);
    }
}
