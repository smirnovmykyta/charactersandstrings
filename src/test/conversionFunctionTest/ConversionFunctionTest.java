package conversionFunctionTest;

import com.gmail.mykyta.smirnov.conversionFunction.ConversionFunction;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class ConversionFunctionTest {
    ConversionFunction conversionFunction = new ConversionFunction();

    @Test
    public void test_integerToString(){
        String expected = "5";

        String actual = conversionFunction.integerToString(5);

        assertEquals(expected, actual);

    }

    @ParameterizedTest
    @ValueSource(ints = {3,5,123,53,123})
    void test_integerToString(int number){
        String expected = Integer.toString(number);

        String actual = conversionFunction.integerToString(number);

        assertEquals(expected,actual);
    }


    @Test
    public void test_doubleToString(){
        String expected = "5.563";

        String actual = conversionFunction.doubleToString(5.563);

        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @ValueSource(doubles = {3.5,5.21,123.1,53.98,123.123})
    void test_doubleToString(double number){
        String expected = Double.toString(number);

        String actual = conversionFunction.doubleToString(number);

        assertEquals(expected,actual);
    }

    @Test
    public void test_stringToInteger(){
        int expected = 5;

        int actual = conversionFunction.stringToInteger("5");

        assertEquals(expected, actual);

    }

    @ParameterizedTest
    @ValueSource(strings = {"3", "5", "123", "53", "123"})
    void test_stringToInteger(String stringOfNumber){
        int expected = Integer.parseInt(stringOfNumber);

        int actual = conversionFunction.stringToInteger(stringOfNumber);

        assertEquals(expected,actual);
    }

    @Test
    public void test_stringToDouble(){
        double expected = 159.753;

        double actual = conversionFunction.stringToDouble("159.753");

        assertEquals(expected, actual, 3.0);
    }

    @ParameterizedTest
    @ValueSource(strings = {"3.5", "5.21", "123.1", "53.98", "123.123"})
    void test_stringToDouble(String stringOfNumber){
        double expected = Double.parseDouble(stringOfNumber);

        double actual = conversionFunction.stringToDouble(stringOfNumber);

        assertEquals(expected,actual);
    }
}
