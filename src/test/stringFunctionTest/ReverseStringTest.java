package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.ReverseString;
import org.junit.Assert;
import org.junit.Test;

public class ReverseStringTest {

    @Test
    public void test_revers(){
        //given
        ReverseString reverseString = new ReverseString();
        String testString = "qwertyuiop";

        //when
        String actual = reverseString.reverse(testString);
        String expected = "poiuytrewq";

        //then
        Assert.assertEquals(expected, actual);
    }
}
