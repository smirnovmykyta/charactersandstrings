package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.SpaceAfterComma;
import org.junit.Assert;
import org.junit.Test;

public class SpaceAfterCommaTest {

    @Test
    public void test_space(){
        //given
        SpaceAfterComma spaceAfterComma = new SpaceAfterComma();
        String str = "Rasd, asda,sadas, asdas,qwd.";

        //when
        String actual = spaceAfterComma.space(str);
        String expected = "Rasd, asda, sadas, asdas, qwd.";

        //then
        Assert.assertEquals(expected, actual);
    }
}
