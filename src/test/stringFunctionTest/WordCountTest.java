package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.WordCount;
import org.junit.Assert;
import org.junit.Test;

public class WordCountTest {

    @Test
    public void test_countingNumberWords(){
        //given
        WordCount wordCount = new WordCount();
        String testString = "Кто понял, тот понял, а кто не понял, тот не понял.";

        //when
        int actual = wordCount.countingNumberWords(testString);
        int expected = 11;

        //then
        Assert.assertEquals(expected,actual);
    }
}
