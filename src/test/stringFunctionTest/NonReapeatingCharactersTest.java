package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.NonReapeatingCharacters;
import org.junit.Assert;
import org.junit.Test;

public class NonReapeatingCharactersTest {

    @Test
    public void test_onlyOneRepeatCharacters(){
        //given
        NonReapeatingCharacters nonReapeatingCharacters = new NonReapeatingCharacters();
        String testString = "abbcccdddfffeeetttyyy";

        //when
        String actual = nonReapeatingCharacters.onlyOneRepeatCharacters(testString).toString();
        String expected = "abcdfety";

        //then
        Assert.assertEquals(expected, actual);

    }
}
