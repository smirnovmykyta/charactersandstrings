package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.RemovePartOfString;
import org.junit.Assert;
import org.junit.Test;

public class RemovePartOfStringTest {

    @Test
    public void test_deletePartString(){
        //given
        RemovePartOfString removePartOfString = new RemovePartOfString();
        String testString = "0123456789";
        //when
        String actual = removePartOfString.deletePartString(testString, 3, 5);
        String expected = "01789";

        //then
        Assert.assertEquals(expected, actual);
    }
}
