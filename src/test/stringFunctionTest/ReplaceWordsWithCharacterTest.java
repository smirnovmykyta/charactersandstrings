package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.ReplaceWordsWithCharacter;
import org.junit.Assert;
import org.junit.Test;

public class ReplaceWordsWithCharacterTest {

    @Test
    public void test_replaceWord(){
        //given
        ReplaceWordsWithCharacter replaceWordWithCharacter = new ReplaceWordsWithCharacter();
        String[] str = new String[]{"aasd", "asknf", "das", "as", "dasdas", "da", "sdas", "dasdas", "das", "das", "dakls", "adukr"};

        //when
        String[] actual = replaceWordWithCharacter.replaceWord(str,5);
        String[] expected = new String[]{"aasd", "as$", "das", "as", "dasdas", "da", "sdas", "dasdas", "das", "das", "da$", "ad$"};

        //then
        Assert.assertArrayEquals(expected,actual);
    }
}
