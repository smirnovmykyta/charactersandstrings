package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.DeleteLastWordInString;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteLastWordInStringTest {

    @Test
    public void test_deleteLastWord(){
        //given
        DeleteLastWordInString deleteLastWordInString = new DeleteLastWordInString();
        String testString = "aasdad asdad asds asdasd asd";

        //when
        String actual = deleteLastWordInString.deleteLastWord(testString);
        String expected = "aasdad asdad asds asdasd";

        //then
        Assert.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @ValueSource(strings = {"aasdad asdad asds asdasd asd", "Prosto Vasya Pupkin", "Prosto losb rogatii"})
    void test_deleteLastWord(String string){


        String actual = DeleteLastWordInString.deleteLastWord(string);
        String expected = STRINGS[actual];

        assertEquals(expected,actual);

    }
}
