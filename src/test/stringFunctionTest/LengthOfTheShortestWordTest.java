package stringFunctionTest;

import com.gmail.mykyta.smirnov.stringFunction.LengthOfTheShortestWord;
import org.junit.Assert;
import org.junit.Test;

public class LengthOfTheShortestWordTest {

    @Test
    public void test_shortestWord(){
        //given
        LengthOfTheShortestWord lengthOfTheShortestWord = new LengthOfTheShortestWord();

        //when
        int actual = lengthOfTheShortestWord.shortestWord("The length of the shortest word in the line, with spaces and commas.");
        int expected = 2;

        //then
        Assert.assertEquals(expected, actual);



    }
}
