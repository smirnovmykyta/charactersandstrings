package symbolInOneLineTest;

import com.gmail.mykyta.smirnov.symbolInOneLine.NumberFromZeroToNine;
import org.junit.Assert;
import org.junit.Test;

public class NumberFromZeroToNineTest {

    @Test
    public void test_numbersFromZeroToNine(){
        NumberFromZeroToNine numberFromZeroToNine = new NumberFromZeroToNine();

        String actual = numberFromZeroToNine.numbersFromZeroToNine();
        String expected = "0 1 2 3 4 5 6 7 8 9 ";

        Assert.assertEquals(expected, actual);
    }
}
