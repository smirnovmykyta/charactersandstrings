package symbolInOneLineTest;

import com.gmail.mykyta.smirnov.symbolInOneLine.CapsAlphabet;
import org.junit.Assert;
import org.junit.Test;

public class CapsAlphabetTest {

    @Test
    public void test_alphabet(){
        CapsAlphabet capsAlphabet = new CapsAlphabet();

        String actual = capsAlphabet.alphabet();
        String expected = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ";

        Assert.assertEquals(expected, actual);
    }
}
