package symbolInOneLineTest;

import com.gmail.mykyta.smirnov.symbolInOneLine.ASCIITable;
import org.junit.Assert;
import org.junit.Test;

public class ASCIITableTest {

    @Test
    public void test_allASCIITable() {
        ASCIITable asciiTable = new ASCIITable();

        String actual = asciiTable.allASCIITable();
        String expected = "  ! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M " +
                "N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ " +
                "\u007F \u0080 \u0081 \u0082 \u0083 \u0084 \u0085 \u0086 \u0087 \u0088 \u0089 \u008A \u008B \u008C " +
                "\u008D \u008E \u008F \u0090 \u0091 \u0092 \u0093 \u0094 \u0095 \u0096 \u0097 \u0098 \u0099 \u009A " +
                "\u009B \u009C \u009D \u009E \u009F   ¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ \u00AD ® ¯ ° ± ² ³ ´ µ ¶ · ¸ ¹ º » ¼ ½" +
                " ¾ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï" +
                " ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ ÿ Ā ā Ă ă Ą ą Ć ć Ĉ ĉ Ċ ċ Č č Ď ď Đ đ ";

        Assert.assertEquals(expected, actual);
    }
}
