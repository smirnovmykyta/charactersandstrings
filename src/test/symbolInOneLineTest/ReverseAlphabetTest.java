package symbolInOneLineTest;

import com.gmail.mykyta.smirnov.symbolInOneLine.ReverseAlphabet;
import org.junit.Assert;
import org.junit.Test;

public class ReverseAlphabetTest {

    @Test
    public void test_reverse(){
        ReverseAlphabet reverseAlphabet = new ReverseAlphabet();
        String expected = "z y x w v u t s r q p o n m l k j i h g f e d c b a ";

        String actual = reverseAlphabet.reverseAlphabet();

        Assert.assertEquals(expected, actual);

    }
}
