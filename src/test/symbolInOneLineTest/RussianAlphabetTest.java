package symbolInOneLineTest;

import com.gmail.mykyta.smirnov.symbolInOneLine.RussianAlphabet;
import org.junit.Assert;
import org.junit.Test;

public class RussianAlphabetTest {

    @Test
    public void test_reverse(){
        RussianAlphabet russianAlphabet = new RussianAlphabet();
        String expected = "а б в г д е ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я ";

        String actual = russianAlphabet.russianAlphabet();

        Assert.assertEquals(expected, actual);

    }
}
